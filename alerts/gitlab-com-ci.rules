## Pending jobs for projects with shared runners enabled
ALERT TooManyPendingBuildsOnSharedRunnerProject
  IF (ci_pending_builds{shared_runners="yes",has_minutes="yes"} > 500)
    and
     (topk(1, predict_linear(ci_pending_builds{shared_runners="yes",has_minutes="yes"}[15m], 3600)) > 1000)
  FOR 5m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="The number of pending builds for projects with shared runners will be too high in 1h: {{$value | printf \"%.2f\" }}",
    description="The number of pending builds for projects with shared runners is increasing and will be too high in 1h ({{$value}}). This may suggest problems with auto-scaling provider or Runner stability. You should check Runner's logs. Check http://performance.gitlab.net/dashboard/db/ci.",
  }


## Pending jobs per namespace over limit
ALERT TooManyPendingJobsPerNamespace
  IF max(ci_pending_builds{shared_runners="yes",namespace!="",has_minutes="yes"}) > 500
  FOR 1m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="Number of pending jobs per namespace too high: {{$value}}",
    description="Number of pending jobs per namespace for projects with shared runners enabled is too high ({{$value}}). Check https://performance.gitlab.net/dashboard/db/ci?panelId=33&fullscreen",
    runbook="troubleshooting/ci_pending_builds.md#2-verify-graphs-and-potential-outcomes-out-of-the-graphs-as-described-in-ci-graphsci_graphsmd"
  }


## Runners manager jobs
ALERT NoJobsOnSharedRunners
  IF sum(ci_runner_builds{job="shared-runners"}) == 0
  FOR 5m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="Number of builds running on shared runners is too low: {{$value}}",
    description="Number of builds running on shared runners for the last 5 minutes is 0. This may suggest problems with auto-scaling provider or Runner stability. You should check Runner's logs. Check http://performance.gitlab.net/dashboard/db/ci.",
  }


## Runners manager status
ALERT RunnersManagerDown
  IF up{job=~"shared-runners|shared-runners-gitlab-org|shared-runners-high-cpu|private-runners|omnibus-runners"} == 0
  FOR 5m
  LABELS {severity="critical", pager="ci-cd"}
  ANNOTATIONS {
    title="Runners manager is down on {{ $labels.instance }}",
    runbook="troubleshooting/runners_manager_is_down.md",
    description="This impacts CI execution builds, consider tweeting: !tweet 'Builds are being delayed due to our shared runners manager being non responsive. We are restarting it to restore the service and then investigating the root cause'. Hosts impacted - {{ $labels.instance }}"
  }


## Machine operations rate
ALERT RunnerMachineCreationRateHigh
  IF sum(ci_docker_machines_provider_machine_states{state="creating"}) / (sum(ci_docker_machines_provider_machine_states{state="idle"}) + 1) > 100
  FOR 1m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="Machine creation rate for runners is too high: {{$value | printf \"%.2f\" }}",
    description="Machine creation rate for the last 1 minute is at least {{$value}} times greater than machines idle rate. This may by a symptom of problems with the auto-scaling provider. Check http://performance.gitlab.net/dashboard/db/ci.",
    runbook="troubleshooting/ci_graphs.md#runners-manager-auto-scaling"
  }


## Runners Cache alerts
ALERT RunnersCacheDown
  IF (probe_success{instance=~"runners-cache.*:(5000/v2|9000/minio/login)"} == 0)
  FOR 5m
  LABELS {severity="critical"}
  ANNOTATIONS {
    title="Runners cache service {{ $labels.instance }} on {{ $labels.fqdn }} has been down for more than 5 minutes.",
    runbook="troubleshooting/runners_cache_is_down.md",
    description="This impacts CI execution builds, consider tweeting: !tweet 'CI executions are being delayed due to our runners cache being down at GitLab.com, we are investigating the root cause'"
  }

ALERT RunnersCacheNginxDown
  IF up{job="runners-cache-server"} == 0
  FOR 5m
  LABELS {severity="critical"}
  ANNOTATIONS {
    title="Runners cache nginx service on {{ $labels.fqdn }} has been down for more than 5 minutes.",
    runbook="troubleshooting/runners_cache_is_down.md",
    description="This impacts CI execution builds, consider tweeting: !tweet 'CI executions are being delayed due to our runners cache being down at GitLab.com, we are investigating the root cause'"
  }

ALERT RunnersCacheServerHasTooManyConnections
  IF node_netstat_Tcp_CurrEstab{fqdn=~"runners-cache-.*.gitlab.com"} > 500
  FOR 20m
  LABELS {severity="critical", pager="ci-cd"}
  ANNOTATIONS {
    title="Number of established connections for {{ $labels.instance }} is too high",
    runbook="troubleshooting/ci_too_many_connections_on_runners_cache_server.md",
    description="This impacts CI execution builds, consider tweeting: !tweet 'CI jobs are being delayed due to issues with Shared Runners cache server. We are restarting it to restore the service.'. Hosts impacted - {{ $labels.instance }}"
  }


## Too many used FDs
ALERT TooManyUsedFDs
  IF process_open_fds{job=~"(omnibus-runners|private-runners|shared-runners|shared-runners-(gitlab-org|high-cpu))"} / process_max_fds{job=~"(omnibus-runners|private-runners|shared-runners|shared-runners-(gitlab-org|high-cpu))"} > 0.8
  FOR 10m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="Number of used file descriptors on {{ $labels.instance }} is too high",
    description="{{ $labels.instance }} is using more than 80% of available FDs since 10 minutes. This may affect Runner's stability. Please look at https://performance.gitlab.net/dashboard/db/ci for more data."
  }


## CI Consul+Prometheus cluster
ALERT DegradatedCIConsulPrometheusCluster
  IF up{fqdn=~"(prometheus|consul)-.*(nyc1|us-east1-(c|d)).*"} == 0
  FOR 5m
  LABELS {severity="warn", channel="ci-cd"}
  ANNOTATIONS {
    title="CI Consul+Prometheus cluster is degradated, {{ $labels.instance }} is down",
    description="One or more of hosts from CI Consul+Prometheus cluster is down: {{ $labels.instance }}."
  }
