ALERT PrometheusUnreachable
  IF up{job=~"prometheus.*"} == 0
  FOR 10m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is unreachable",
    description = "{{$labels.job}} at {{$labels.instance}} could not be scraped for over 10 minutes.",
    runbook = "troubleshooting/prometheus-is-down.md",
  }

ALERT PrometheusManyRestarts
  IF changes(process_start_time_seconds{job=~"prometheus.*"}[30m]) > 3
  FOR 30m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is restarting frequently",
    description = "{{$labels.job}} at {{$labels.instance}} has restarted more than 3 times in the last 30 minutes. It might be crashlooping.",
    runbook = "troubleshooting/prometheus-is-down.md",
  }

ALERT PrometheusManyFileSDReadErrors
  IF
      rate(prometheus_sd_file_read_errors_total{job=~"prometheus.*"}[5m])
    /
      rate(prometheus_sd_file_scan_duration_seconds_count{job=~"prometheus.*"}[5m])
    * 100
    > 5
  FOR 10m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} has many DNS-SD errors",
    description = "{{$labels.job}} at {{$labels.instance}} has {{$value}}% of DNS-SD requests failing.",
    runbook = "troubleshooting/prometheus-file-sd-errors.md",
  }

ALERT PrometheusRuleEvaluationSlow
  IF prometheus_evaluator_duration_seconds{quantile="0.9",job=~"prometheus.*"} > 60
  FOR 10m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is evaluating rules too slowly",
    description = "{{$labels.job}} at {{$labels.instance}} has a 90th percentile latency of {{$value}}s completing rule evaluation cycles.",
    runbook = "troubleshooting/prometheus-slow-rule-eval.md",
  }

ALERT PrometheusCheckpointingSlow
  IF
      avg_over_time(prometheus_local_storage_checkpoint_last_duration_seconds{job=~"prometheus.*"}[15m])
    >
      prometheus_local_storage_max_chunks_to_persist{job=~"prometheus.*"} / 5000  # Allow 200µs per chunk.
  FOR 5m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is checkpointing too slowly",
    description = "{{$labels.job}} at {{$labels.instance}} needs {{$value}}s on average for each checkpoint.",
    runbook = "troubleshooting/prometheus-indexing-backlog.md",
  }

ALERT PrometheusIndexingBacklog
  IF
      prometheus_local_storage_indexing_queue_length{job=~"prometheus.*"}
    /
      prometheus_local_storage_indexing_queue_capacity{job=~"prometheus.*"}
    * 100
    > 10
  FOR 30m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is backlogging on the indexing queue",
    description = "{{$labels.job}} at {{$labels.instance}} is backlogging on the indexing queue for more than 30m. Queue is currently {{$value | printf `%.0f`}}% full.",
    runbook = "troubleshooting/prometheus-indexing-backlog.md",
  }

ALERT PrometheusNotIngestingSamples
  IF rate(prometheus_local_storage_ingested_samples_total{job=~"prometheus.*"}[5m]) == 0
  FOR 5m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is not ingesting samples",
    description = "{{$labels.job}} at {{$labels.instance}} has not ingested any samples in the last 10 minutes.",
    runbook = "troubleshooting/prometheus-not-ingesting.md",
  }

ALERT PrometheusPersistErrors
  IF rate(prometheus_local_storage_persist_errors_total{job=~"prometheus.*"}[10m]) > 0
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} has persist errors",
    description = "{{$labels.job}} at {{$labels.instance}} has encountered {{$value}} persist errors per second in the last 10 minutes.",
    runbook = "troubleshooting/prometheus-persist-errors.md",
  }

ALERT PrometheusNotificationsBacklog
  IF prometheus_notifications_queue_length{job=~"prometheus.*"} > 0
  FOR 10m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is backlogging on the notifications queue",
    description = "{{$labels.job}} at {{$labels.instance}} is backlogging on the notifications queue. The queue has not been empty for 10 minutes. Current queue length: {{$value}}.",
    runbook = "troubleshooting/prometheus-notifications-backlog.md",
  }

ALERT PrometheusScrapingSlowly
  # The match in the interval label excludes any intervals >= 1m.
  IF prometheus_target_interval_length_seconds{quantile="0.9",interval!~".*m.*",job=~"prometheus.*"} > 2 * 60
  FOR 10m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is scraping targets slowly",
    description = "{{$labels.job}} at {{$labels.instance}} has a 90th percentile latency of {{$value}}s for scraping targets in the {{$labels.interval}} target pool.",
    runbook = "troubleshooting/prometheus-slow-scrapes.md",
  }

ALERT PrometheusStorageInconsistent
  IF prometheus_local_storage_inconsistencies_total{job=~"prometheus.*"} > 0
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} has an inconsistent storage",
    description = "{{$labels.job}} at {{$labels.instance}} has detected a storage inconsistency. A server restart is needed to initiate recovery.",
    runbook = "troubleshooting/prometheus-storage-inconsistent.md",
  }

ALERT PrometheusPersistencePressureTooHigh
  IF
      prometheus_local_storage_persistence_urgency_score{job=~"prometheus.*"} > 0.8
    AND
      predict_linear(prometheus_local_storage_persistence_urgency_score{job=~"prometheus.*"}[30m], 3600 * 24) > 1
  FOR 30m
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} can not keep up persisting",
    description = "{{$labels.job}} at {{$labels.instance}} is approaching critical persistence pressure. Throttled ingestion expected within the next 24h.",
    runbook = "troubleshooting/prometheus-persistence-pressure-high.md",
  }

ALERT PrometheusPersistencePressureTooHigh
  IF
      prometheus_local_storage_persistence_urgency_score{job=~"prometheus.*"} > 0.85
    AND
      predict_linear(prometheus_local_storage_persistence_urgency_score{job=~"prometheus.*"}[30m], 3600 * 2) > 1
  FOR 30m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} can not keep up persisting",
    description = "{{$labels.job}} at {{$labels.instance}} is approaching critical persistence pressure. Throttled ingestion expected within the next 2h.",
    runbook = "troubleshooting/prometheus-persistence-pressure-high.md",
  }

ALERT PrometheusSeriesMaintenanceStalled
  IF
        prometheus_local_storage_memory_series{job=~"prometheus.*"}
      / on(job,instance)
        rate(prometheus_local_storage_series_ops_total{type="maintenance_in_memory",job=~"prometheus.*"}[5m])
      / 3600
      > 24
    AND on(job,instance)
      prometheus_local_storage_rushed_mode == 1

  FOR 1h
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is maintaining memory time series too slowly",
    description = "{{$labels.job}} at {{$labels.instance}} is maintaining memory time series so slowly that it will take {{$value | printf `%.0f`}}h to complete a full cycle. This will lead to persistence falling behind.",
    runbook = "troubleshooting/prometheus-slow-series-maintenance.md",
  }

ALERT PrometheusInvalidConfigFile
  IF prometheus_config_last_reload_successful{job=~"prometheus.*"} == 0
  FOR 30m
  LABELS {
    service = "prometheus",
    severity = "critical",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} has an invalid config",
    description = "The configuration file for {{$labels.job}} at {{$labels.instance}} is invalid and was therefore not reloaded.",
    runbook = "troubleshooting/prometheus-invalid-config.md",
  }

ALERT PrometheusOutOfOrderSamplesDiscarded
  IF increase(prometheus_local_storage_out_of_order_samples_total{job=~"prometheus.*"}[10m]) > 0
  FOR 1h
  LABELS {
    service = "prometheus",
    severity = "warn",
  }
  ANNOTATIONS {
    title = "{{$labels.job}} is discarding out-of-order samples",
    description = "{{$labels.job}} at {{$labels.instance}} has discarded {{$value}} out-of-order samples over the last hour.",
    runbook = "troubleshooting/prometheus-out-of-order.md",
  }
